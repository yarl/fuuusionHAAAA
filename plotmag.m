pkg load mapping;

clc
close
clear
%récupération des données de calibration générées avec magcal.m
cal = csvread("magcal.data");

biascal = cal(1:3);
scalecal = cal(4:6);

dir = 0;

while true
%lecture fifo
  f = fopen("fif"); A = csvread(f);
%application de la calibration
  x = (mean(A(:,1)) - biascal(1)) * scalecal(1);
  y = (mean(A(:,2)) - biascal(2)) * scalecal(2);
  z = (mean(A(:,3)) - biascal(3)) * scalecal(3);

%angle latéral
  a = atan2(x, y);
%application de la declinaison magnétique, récupérée https://www.ngdc.noaa.gov/geomag-web/#igrfwmm, par exemple
  a = a - deg2rad(dms2degrees([0 34 58]));
  figure(1);
  clf;
  compass(cos(a), sin(a));
  drawnow();
  
%angle vertical
  b = atan2(V(3),V(1));
% application de l'inclinaison magnétique
  b = b + deg2rad(dms2degrees([62 35 37]));
  figure(2);
  clf;
  compass(cos(b), sin(b));
  drawnow();
endwhile
