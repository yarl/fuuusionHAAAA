clc
close
clear

% Simple hard and soft iron calibration based on https://github.com/kriswiner/MPU-6050/wiki/Simple-and-Effective-Magnetometer-Calibration

A = csvread("mdata");

x = A(:,1);
y = A(:,2);
z = A(:,3);

figure(1)
plot(x,y,".r;Mag-xy;",y,z,".g;Mag-yz;",z,x,".b;Mag-zx;")
title("data: hard and soft iron biases")

maxx = max(x)
minx = min(x)
maxy = max(y)
miny = min(y)
maxz = max(z)
minz = min(z)

biasx = (maxx + minx)/2
biasy = (maxy + miny)/2
biasz = (maxz + minz)/2

x = x - biasx;
y = y - biasy;
z = z - biasz;

figure(2)
plot(x,y,".r;Mag-xy;",y,z,".g;Mag-yz;",z,x,".b;Mag-zx;")
title("data: soft iron biases")

scalex = (maxx - minx)/2
scaley = (maxy - miny)/2
scalez = (maxz - minz)/2

avgscale = (scalex + scaley + scalez)/3

scalex = avgscale/scalex;
scaley = avgscale/scaley;
scalez = avgscale/scalez;

x = x * scalex;
y = y * scaley;
z = z * scalez;

figure(3)
plot(x,y,".r;Mag-xy;",y,z,".g;Mag-yz;",z,x,".b;Mag-zx;")
title("data: no longer biases")

csvwrite("magcal.data", [biasx,biasy,biasz,scalex,scaley,scalez]);